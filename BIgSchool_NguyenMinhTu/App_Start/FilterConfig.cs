﻿using System.Web;
using System.Web.Mvc;

namespace BIgSchool_NguyenMinhTu
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
