﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BIgSchool_NguyenMinhTu.Startup))]
namespace BIgSchool_NguyenMinhTu
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
